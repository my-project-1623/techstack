import actionTypes from '../actionTypes';

export const selectLibraryId = (payload) => {
    return {
        type: actionTypes.SELECT_LIBRARY_ID,
        payload,
    };
};
