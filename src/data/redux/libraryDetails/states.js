import libraries from '../../config/libraryData.json';

const states = {
    libraryDetails: {
        libraries,
        selectedLibraryId: null,
    }
};

export default states;
