import actionTypes from '../actionTypes';
import states from './states';

const libraryDetails = (state = states.libraryDetails, action) => {
    switch (action.type) {
        case actionTypes.SELECT_LIBRARY_ID:
            return {
                ...state,
                selectedLibraryId: state.selectedLibraryId && action.payload === state.selectedLibraryId ? null : action.payload
            };

        default:
            return state;
    }
};

export default libraryDetails;

