import { combineReducers } from 'redux';

import libraryDetails from './libraryDetails/reducers';

const rootReducer = combineReducers({
    libraryDetails,
});

export default rootReducer;
