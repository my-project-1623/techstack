import { createStore } from 'redux';
import rootReducer from './rootReducer';

const createReduxStore = () => { 
    const store = createStore(rootReducer);
    return store;
};

export default createReduxStore;

