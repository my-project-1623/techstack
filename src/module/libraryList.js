import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FlatList, View } from 'react-native';
import { bindActionCreators } from 'redux';

import * as libraryActions from '../data/redux/libraryDetails/actions';
import ListItem from '../components/listItem';

class LibraryList extends Component {
    renderListItem = ({ item }) => {
        const { actions } = this.props;
        return (
            <ListItem
                library={item}
                onPress={() => actions.selectLibraryId(item.id)}
            />
        );
    };

    render() {
        const { libraryDetails } = this.props;
        return (
            <View style={{ padding: 10 }}>
                <FlatList data={libraryDetails.libraries} renderItem={this.renderListItem} />
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        libraryDetails: state.libraryDetails
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Object.assign({}, libraryActions), dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(LibraryList);
