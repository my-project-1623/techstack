import React, { Component } from 'react';
import { View } from 'react-native';
import { Provider } from 'react-redux';

import createReduxStore from './data/redux/store';
import { Header } from './components/common';
import LibraryList from './module/libraryList';

const store = createReduxStore();
export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <View style={{ flex: 1 }}>
          <Header title='Tech Stack' />
          <LibraryList />
        </View>
      </Provider>
    );
  }
}

