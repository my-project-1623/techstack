import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, TouchableWithoutFeedback, LayoutAnimation } from 'react-native';

class ListItem extends Component {
    componentWillUpdate() {
        LayoutAnimation.easeInEaseOut();
    }

    renderDescription = () => {
        const { expand, library } = this.props;
        if (expand) {
            return (
                <View style={styles.descriptionStyle}>
                    <Text>{library.description}</Text>
                </View>
            );
        }
        return null;
    };

    render() {
        const { viewStyle, textStyle } = styles;
        const { library, onPress } = this.props;

        return (
            <TouchableWithoutFeedback onPress={onPress}>
                <View style={viewStyle}>
                    <Text style={textStyle}>{library.title}</Text>
                    {this.renderDescription()}
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        expand: state.libraryDetails.selectedLibraryId === ownProps.library.id
    };
}

export default connect(mapStateToProps)(ListItem);

const styles = {
    viewStyle: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        marginBottom: 5,
        paddingBottom: 5,
        borderWidth: 1,
        borderColor: 'lightgrey',
        borderRadius: 3,
    },
    textStyle: {
        fontSize: 18,
        textTransform: 'capitalize',
    },
    descriptionStyle: {
        paddingTop: 5,
        marginTop: 5,
        borderTopWidth: 0.5,
        borderColor: 'lightgrey'
    }
};
