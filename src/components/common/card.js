import React from 'react';
import { View } from 'react-native';

const Card = ({ children }) => {
    const { viewStyle } = styles;
    return (
        <View style={viewStyle}>
            {children}
        </View>
    );
};

export { Card };

const styles = {
    viewStyle: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        borderRadius: 3,
        backgroundColor: '#fff',
    },
};
